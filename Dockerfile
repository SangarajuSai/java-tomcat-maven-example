FROM tomcat:8.0

WORKDIR /app

ADD **/*.war /usr/local/tomcat/webapps/

EXPOSE 8087

CMD ["catalina.sh", "run"]
